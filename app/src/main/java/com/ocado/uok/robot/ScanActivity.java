package com.ocado.uok.robot;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.ocado.uok.skilo.scanner.ScannerResult;

import static com.ocado.uok.robot.R.layout;

/**
 * Ekran ze skanowaniem produktów.
 */
public class ScanActivity extends ActivityWithSomeMethods {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_scan);

        ScannerResult scannerResult =
                ViewModelProviders.of(this).get(ScannerResult.class);

        scannerResult.getLastScanned().observe(this, scannedText -> {
            showToast(scannedText);
        });
    }

}
