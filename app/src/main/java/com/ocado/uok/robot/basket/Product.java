package com.ocado.uok.robot.basket;


import com.ocado.uok.robot.R;

import java.util.ArrayList;

/**
 * Produkt wraz ze zdjęciem
 */
public enum Product {

    APPLE(R.drawable.ic_apple),
    BANANA(R.drawable.ic_banana),
    ORANGE(R.drawable.ic_orange),
    ONION(R.drawable.ic_onion),
    CUCUMBER(R.drawable.ic_cucumber);

    private int id;

    Product(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Product of(String text) {
        try {
            return Product.valueOf(text.toUpperCase());
        }
        catch (IllegalArgumentException ex) {
            System.err.println("Illegal argument " + text);
            return null;
        }
    }

    public static boolean firstContainsSecond(ArrayList<Product> first, ArrayList<Product> second) {
        ArrayList<Product> firstClonned = (ArrayList<Product>) first.clone();
        for (Product p: second) {
            if (!firstClonned.remove(p)) {
                return false;
            }
        }
        return true;
    }
}
